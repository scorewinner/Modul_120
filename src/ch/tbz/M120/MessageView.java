package ch.tbz.M120;

import ch.tbz.M120.model.Message;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * This class is used to display a message
 */
public class MessageView extends VBox {
    //Creating two Nodes
    Label author;
    Label content;

    //Constructor of MessageView
    public MessageView(Message message){
        setBackground(new Background(new BackgroundFill(Color.GREY, null, null)));
        author = new Label(message.contact().name() + ":");
        author.setTextFill(Color.WHITE);
        author.setFont(Font.font(16));
        author.setStyle("color: white");
        getChildren().add(author);
        content = new Label(message.content());
        content.setTextFill(Color.WHITE);
        getChildren().add(content);
    }
}
