package ch.tbz.M120;

import ch.tbz.M120.model.Chat;
import ch.tbz.M120.model.Model;
import ch.tbz.M120.model.dummy.DummyModel;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Main extends Application {

    //Setting up nodes and objects
    Model model;
    HBox layout;
    VBox leftPane;
    HBox chatControls;
    TabPane voiceChatTabs;
    Tab voiceTab;
    Tab textTab;
    TextField chatSearch;
    ChatView chatView;
    VBox chatBar;
    Scene scene;


    @Override
    //configure all the components and bring them up together
    public void start(Stage primaryStage) throws Exception {
        model = new DummyModel();

        primaryStage.setTitle("ChatClient");

        layout = new HBox();

        leftPane = new VBox();

        chatControls = new HBox();
        chatSearch = new TextField();
        chatSearch.promptTextProperty().setValue("search contacts");
        chatControls.getChildren().add(chatSearch);


        chatBar = new VBox();
        loadChatBar("");


        leftPane.getChildren().add(chatControls);
        leftPane.getChildren().add(chatBar);
        layout.getChildren().add(leftPane);

        voiceChatTabs = new TabPane();
        voiceTab = new Tab("voice");
        voiceTab.closableProperty().setValue(false);
        voiceChatTabs.getTabs().add(voiceTab);
        layout.getChildren().add(voiceChatTabs);
        HBox.setHgrow(voiceChatTabs, Priority.ALWAYS);

        textTab = new Tab("text");
        chatView = new ChatView();
        textTab.closableProperty().setValue(false);
        textTab.setContent(chatView);
        voiceChatTabs.getTabs().add(textTab);

        scene = new Scene(layout, 1280, 720);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    // method is called in the beginning and whenever a button one the chat bar is pressed
    public void loadChatBar(String active) {
        chatBar.getChildren().clear(); // clears all buttons from the chat bar
        for (Chat chat: model.chats()){
            ToggleButton button = new ToggleButton(chat.name());

            //checks whether the current chat is the active chat and if so sets the button to selected
            if (chat.name().equals(active)) {
                button.setSelected(true);
            }

            //makes sure the button fills the available space
            button.setMaxWidth(Double.MAX_VALUE);
            VBox.setVgrow(button, Priority.ALWAYS);


            chatBar.getChildren().add(button); // adds the button to chat bar

            // adds an event handler to the button so that if pressed the respective chat is loaded
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    /*reloads the chat bar, so that no other button is selective (given more time we would have handled
                    this differently)*/
                    loadChatBar(chat.name());

                    loadChat(chat);
                }
            });
        }
    }

    // loads whatever chat is passed to it to the chat view
    public void loadChat(Chat chat) {
        chatView.reset(chat);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
