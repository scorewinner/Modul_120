package ch.tbz.M120.model;

/**
 * Each instance of this Class represents a chat contact.
 */
public interface Contact {
    String name();
}
