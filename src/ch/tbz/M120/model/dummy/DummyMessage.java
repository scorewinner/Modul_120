package ch.tbz.M120.model.dummy;

import ch.tbz.M120.model.Contact;
import ch.tbz.M120.model.Message;

/**
 * A dummy implementation of the Message interface
 */
public class DummyMessage implements Message {
    public Contact contact;
    public String content;

    //Constructor of DummyMessage
    public DummyMessage(Contact contact, String content) {
        this.contact = contact;
        this.content = content;
    }

    @Override
    public Contact contact(){
        return contact;
    }

    @Override
    public String content(){
        return content;
    }
}
