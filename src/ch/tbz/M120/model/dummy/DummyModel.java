package ch.tbz.M120.model.dummy;

import ch.tbz.M120.model.Chat;
import ch.tbz.M120.model.Contact;
import ch.tbz.M120.model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * A dummy implementation of the Model interface.
 */
public class DummyModel implements Model {
    DummyContact self;
    ArrayList<DummyContact> contacts;
    ArrayList<DummyChat> chats;

    //constructor of DummyModel
    public DummyModel() {
        self = new DummyContact("You");
        contacts = new ArrayList<>();
        chats = new ArrayList<>();
        //Creating Chats
        chats.add(new DummyChat("some chat", self, contacts));
        chats.add(new DummyChat("chat 1", self, contacts));
        chats.add(new DummyChat("chat 2", self, contacts));
        chats.add(new DummyChat("hello", self, contacts));
    }

    @Override
    public List<Contact> contacts(){
        return new ArrayList<>(contacts);
    }

    @Override
    public List<Chat> chats(){
        return new ArrayList<>(chats);
    }
}
