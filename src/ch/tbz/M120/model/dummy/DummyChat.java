package ch.tbz.M120.model.dummy;

import ch.tbz.M120.model.Chat;
import ch.tbz.M120.model.Contact;
import ch.tbz.M120.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * A dummy implementation of the Chat interface.
 */
public class DummyChat implements Chat {
    String name;
    Contact self;
    ArrayList<DummyContact> members;
    ArrayList<DummyMessage> messages;

    //constructor of DummyChat
    public DummyChat(String name, Contact self, ArrayList<DummyContact> members) {
        this.name = name;
        this.self = self;
        this.members = members;
        this.messages = new ArrayList<>();
    }

    @Override
    //returns string with the name
    public String name() {
        return name;
    }

    @Override
    //adds a text to the arrayList messages
    public void write(String text) {
        messages.add(new DummyMessage(self, text));
    }

    @Override
    //returns a List with the members
    public List<Contact> members(){
        return new ArrayList<>(members);
    }

    @Override
    //returns a new List with messages
    public List<Message> messages(){
        return new ArrayList<>(messages);
    }
}
