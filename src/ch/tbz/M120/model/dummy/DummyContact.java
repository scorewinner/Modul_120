package ch.tbz.M120.model.dummy;

import ch.tbz.M120.model.Contact;
import javafx.scene.image.Image;

/**
 * A dummy implementation of the Contact interface.
 */
public class DummyContact implements Contact {
    public String name;

    //Constructor of DummyContact
    public DummyContact(String name) {
        this.name = name;
    }

    @Override
    public String name(){
        return name;
    }
}
