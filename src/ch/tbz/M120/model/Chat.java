package ch.tbz.M120.model;

import java.util.List;

/**
 * A Chat contains it's members and the messages sent. There is no difference between a group chat and a chat with only
 * one contact, this allows you to have multiple chats with only one person.
 */
public interface Chat {
    String name();
    void write(String text);
    List<Contact> members();
    List<Message> messages();
}
