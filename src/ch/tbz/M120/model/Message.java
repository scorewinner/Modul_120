package ch.tbz.M120.model;

/**
 * A message has to be sent from a contact and it has to have some content.
 */
public interface Message {
    Contact contact();
    String content();
}
