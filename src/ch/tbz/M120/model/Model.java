package ch.tbz.M120.model;

import java.util.List;

/**
 * The model interface: each model has to have a list of contacts and chats.
 */
public interface Model {
    List<Contact> contacts();
    List<Chat> chats();
}
