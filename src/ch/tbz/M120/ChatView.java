package ch.tbz.M120;

import ch.tbz.M120.model.Chat;
import ch.tbz.M120.model.Message;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class ChatView extends VBox {
    //Setting up needed Nodes
    Chat chat;
    ScrollPane messagesPane;
    VBox messagesBox;
    ArrayList<MessageView> messages;
    HBox textSend;
    TextField textField;
    Button sendButton;

    //Constructor of ChatView
    public ChatView(Chat chat) {
        this.chat = chat;

        messages = new ArrayList<>();

        for(Message message: chat.messages()) {
            messages.add(new MessageView(message));
        }

        messagesPane = new ScrollPane();
        messagesBox = new VBox(16);
        for(MessageView message: messages) {
            messagesBox.getChildren().add(message);
        }
        messagesPane.setContent(messagesBox);
        VBox.setVgrow(messagesPane, Priority.ALWAYS);
        getChildren().add(messagesPane);

        textSend = new HBox();
        textField = new TextField();
        textField.promptTextProperty().setValue("write message");
        HBox.setHgrow(textField, Priority.ALWAYS);
        textSend.getChildren().add(textField);
        sendButton = new Button("send");
        sendButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                chat.write(textField.getText());
                reset(chat);
            }
        });
        textSend.getChildren().add(sendButton);
        getChildren().add(textSend);
    }

    public ChatView(){

    }

    public void reset(Chat chat) {
        this.chat = chat;

        getChildren().clear();

        messages = new ArrayList<>();

        for(Message message: chat.messages()) {
            messages.add(new MessageView(message));
        }

        messagesPane = new ScrollPane();
        messagesBox = new VBox(16);
        for(MessageView message: messages) {
            messagesBox.getChildren().add(message);
        }
        messagesPane.setContent(messagesBox);
        VBox.setVgrow(messagesPane, Priority.ALWAYS);
        getChildren().add(messagesPane);

        textSend = new HBox();
        textField = new TextField();
        textField.promptTextProperty().setValue("write message");
        HBox.setHgrow(textField, Priority.ALWAYS);
        textSend.getChildren().add(textField);
        sendButton = new Button("send");
        sendButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                chat.write(textField.getText());
                reset(chat);
            }
        });
        textSend.getChildren().add(sendButton);
        getChildren().add(textSend);
    }
}
